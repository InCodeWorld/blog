<?php
namespace app\admin\controller;

use think\Controller;
use think\Request;
use think\Session;

class Common extends Controller
{
    public function __construct()
    {
        parent::__construct();
        $c = Session::get('session_start_time');
        $v = time() - Session::get('session_start_time');
        $isOverdue  = ((time() - Session::get('session_start_time')) > config('session.expire'));
        $isMissed = Session::has('nickname');
        if ($isOverdue) {
            if($isMissed){
                unset($_SESSION['nickname']);
            }
            if (Request::instance()->isAjax()) {
                echo "timeout";
                exit;
             } else {
                $this->redirect('Home/library/index', ['isLogin' => 1]) ;
                die;
             }
        } else {
            Session::set('session_start_time', time());
        }
    }
}