<?php
namespace app\admin\controller;
use think\Request;
class Error
{
    public function index(Request $request)
    {
        return redirect('admin/library/index');
    }

    public function _empty($name)
    {
        return redirect('admin/library/index');
    }
}