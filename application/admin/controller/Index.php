<?php
namespace app\admin\controller;

use app\admin\model\Article;
use app\admin\model\Catalog;
use app\admin\model\BookShelf;
use app\admin\model\BookCover;

class Library extends Common
{
    private $bookshelf;
    private $bookcover;
    private $catalog;
    private $article;

    public function __construct()
    {
        parent::__construct();
        $this->bookshelf = new BookShelf();
        $this->bookcover = new BookCover();
        $this->catalog = new Catalog();
        $this->article = new Article();
    }

    public function index()
    {
        $bookshelf_info = $this->bookshelf->order('id')->select();
        $bookcover_info = $this->bookcover->order('id')->select();
        $this->assign('bookshelf_info', $bookshelf_info);
        $this->assign('bookcover_info', $bookcover_info);
        return $this->fetch();
    }
}