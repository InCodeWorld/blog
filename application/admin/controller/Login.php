<?php
namespace app\admin\controller;

use app\admin\model\Admin;
use think\Controller;
use think\Session;

class Login extends Controller
{
    private $admin;

    public function __construct()
    {
        parent::__construct();
        $this->admin = new Admin();
    }

    public function index()
    {
        $data = input();
        $validate_result = $this->validate($data, 'Users');
        if (true !== $validate_result) {
            $this->error($validate_result);
        } else {
            $data['pwd'] = getPwd($data['pwd']);
            $data = model('admin')->where(['username' => $data['nickname'], 'pwd'=>$data['pwd']])->find();
            if ($data) {
                Session::set('nickname', $data['username']);
                Session::set('pwd', $data['pwd']);
                Session::set('last_login_time', $data['login_time']);
                Session::set('session_start_time', time());
                model('admin')->save(['login_time' => time()], ['id' => $data['id']]);
                $this->success('登录成功', url('admin/library/index'), 3000);
            } else {
                $this->error('用户名或密码错误', 3000);
            }
        }
        $this->redirect('admin/library/index');
    }

    private function tempGetPwd($pwd)
    {
        echo getPwd($pwd);
    }
}