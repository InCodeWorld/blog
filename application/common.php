<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +-------------------------------------------------------------------

function arrayToObject($arr,&$obj){
    foreach ($arr as $key=>$val){
        $obj->$key=$val;
    }
}

// 应用公共文件
function getTree($data,$parentid=0,$level=0){
    static  $_ret = array();
    foreach($data as $k=>$v){
        if($v['parentid'] == $parentid){
            $v['level'] = $level;
            $_ret[] = $v;
            getTree($data,$v['id'],$level+1);
        }
    }
    return $_ret;
}

function getChildren($data, $id, $isClear=FALSE)
{
    static $ret=array();
    if ($isClear==true)
        $ret=array();
    foreach ($data as $k=>$v)
    {
        if($v['parentid']==$id)
        {
            $ret[]=$v['id'];
            getChildren($data, $v['id']);
        }
    }
    return $ret;
}

function getParentTree($data,$id,$isClear=False)
{
    static $_arr = [];
    if ($isClear) {
        $_arr = [];
    }
    foreach ($data as $k => $v) {
        if($id==0)break;
        if ($v['id'] == $id) {
            $_arr[] = $v["parentid"];
            getParentTree($data,$v["parentid"]);
        }
    }
    return $_arr;
}

/** 删除所有空目录  * @param String $path 目录路径  */
function rm_empty_dir($path){
    if(is_dir($path) && ($handle = opendir($path))!==false){
        while(($file=readdir($handle))!==false){// 遍历文件夹
            if($file!='.' && $file!='..'){
                $curfile = $path.'/'.$file;// 当前目录
                if(is_dir($curfile)){// 目录
                    rm_empty_dir($curfile);// 如果是目录则继续遍历
                    if(count(scandir($curfile))==2){//目录为空,=2是因为.和..存在
                        rmdir($curfile);// 删除空目录
                    }
                }
            }
        }
        closedir($handle);
    }
}

/**
 * 删除目录及目录下所有文件或删除指定文件
 * @param str $path   待删除目录路径
 * @param int $delDir 是否删除目录，1或true删除目录，0或false则只删除文件保留目录（包含子目录）
 * @return bool 返回删除状态
 */
function delDirAndFile($path, $delDir = True) {
    if(!file_exists($path)){
        return false;
    }
    $handle = opendir($path);
    if ($handle) {
        while (false !== ( $item = readdir($handle) )) {
            if ($item != "." && $item != "..")
                is_dir("$path/$item") ? delDirAndFile("$path/$item", $delDir) : unlink("$path/$item");
        }
        closedir($handle);
        if ($delDir)
            return rmdir($path);
    }else {
        if (file_exists($path)) {
            return unlink($path);
        } else {
            return FALSE;
        }
    }
}

/**
 * @Author   LaoYang
 * @DateTime 2017-05-16
 * @param    [string]     $pwd [需要加密的明文密码]
 * @return   [string]          [加密之后的密码]
 */
function getPwd($pwd){
    $key = substr(sha1('xinjiang'),9,10);
    $pwd = substr(sha1($pwd),19,10);
    $pwd = sha1($key . $pwd);
    return $pwd;
}
