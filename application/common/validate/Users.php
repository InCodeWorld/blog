<?php
namespace app\common\validate;

use think\Validate;

class Users extends Validate
{
    // 验证规则
    protected $rule = [
        ['nickname', 'require|min:6|max:12|alphaDash', '昵称错误|昵称错误|昵称错误|昵称错误'],
        ['pwd', 'require|length:8:12', '密码错误|密码错误'], // 更多 内置规则 http://www.kancloud.c
        ['captcha', 'require|length:4|captcha', '验证码错误|验证码错误|验证码错误'],
    ];

    // 验证邮箱格式 是否符合指定的域名
    protected function checkMail($value, $rule)
    {
        $result = strstr($value, $rule);
        if ($result)
            return true;
        else
            return "邮箱必须包含 $rule 域名";
    }
}