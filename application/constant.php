<?php

define('APP_PATH', __DIR__ . '/../application/');
define('STATIC_PATH', '/static/');
define('ADMIN_STATIC', '/static/admin/');
define('HOME_STATIC', '/static/home/');
define('EDITOR_PATH', '/static/editor.md-master/');
//错误代码
define ('E_OK',                        0);// 成功
define ('E_MISS_PARAM',                1);// 缺少参数
define ('E_PARAM',                     2);// 参数格式错误
define ('E_SESSION_FAILED',            3);// 请求session 失败
define ('E_AUTH',                      4);// 没有登陆管理账号
define ('E_PERMISSION',                5);// 没有权限
define ('E_IP_NOT_ALLOWED',            6);// 该IP 不允许调用此接口
define ('E_SQL',                       7);// SQL 错误
define ('E_ITEM_NOT_EXIST',            8);// 未找到对应的数据库条目
define ('E_ITEM_ALREADY_EXIST',        9);// 要新增的条目已经存在
define ('E_ADD_ITEM_FAILED',           10);// 新增条目失败
define ('E_UPDATE_ITEM_FAILED',        11);// 更新条目失败
define ('E_DELETE_ITEM_FAILED',        12);// 刪除条目失败
define ('E_REQUEST_FAILED',            13);// 请求失败
define ('E_UPLOAD_FAILED',             14);// 上传失败
define ('E_UNLOAD_FAILED',             14);// 卸载失败
define ('E_NOT_SUPPORTED_ANYMORE',     15);// 接口不再支持
define ('E_LOGIC',                     16);// 逻辑错误
define ('E_CONFIG',                    17);// 配置错误
define ('E_MISS_CONFIG',               18);// 缺少配置项目
define ('E_MISS_LOG_DIR',              19);// 缺少日志文件目录
define ('E_EXEC_SQL_FAILED',           20);// 执行SQL 失败
define ('E_CUSTOM',                    1000);// 自定义错误
