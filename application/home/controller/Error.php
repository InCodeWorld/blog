<?php
namespace app\home\controller;
use think\Request;
class Error
{
    public function index(Request $request)
    {
        return redirect('home/library/index');
    }

    public function _empty($name)
    {
        return redirect('home/library/index');
    }
}