<?php
namespace app\home\controller;

use think\Controller;

class Game extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        return $this->fetch();
    }

    public function _empty($name)
    {
        $this->redirect('home/library/index');
    }
}