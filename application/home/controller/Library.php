<?php
namespace app\home\controller;

use app\home\model\Article;
use app\home\model\Catalog;
use think\Controller;
use app\home\model\BookShelf;
use app\home\model\BookCover;

class Library extends Controller
{
    private $bookshelf;
    private $bookcover;
    private $catalog;
    private $article;

    public function __construct()
    {
        parent::__construct();
        $this->bookshelf = new BookShelf();
        $this->bookcover = new BookCover();
        $this->catalog = new Catalog();
        $this->article = new Article();
    }

    public function index()
    {
        $bookshelf_info = $this->bookshelf->order('id')->select();
        $bookcover_info = $this->bookcover->order('id')->select();
        $this->assign('bookshelf_info', $bookshelf_info);
        $this->assign('bookcover_info', $bookcover_info);
        if(input('param.isLogin')==1){
            $this->assign('login', 1);
        } else {
            $this->assign('login', 0);
        }
        return $this->fetch();
    }

    public function _empty($name)
    {
        $this->redirect('home/library/index');
    }
}