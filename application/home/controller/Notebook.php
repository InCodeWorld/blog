<?php
namespace app\home\controller;

use app\home\model\Article;
use app\home\model\Catalog;
use think\Controller;
use think\Request;

class Notebook extends Controller
{
    private $catalog;
    private $article;

    public function __construct()
    {
        parent::__construct();
        $this->catalog = new Catalog();
        $this->article = new Article();
    }

    public function index()
    {
        if (Request::instance()->isGet()) {
            $get = input('get.');
            try {
                $catelog_info = $this->catalog->where([
                    'cover_id' => $get['coverid'],
                    'parent_id' => $get['parentid'],
                ])->select();
                $article_info = $this->article->field('id,article_title,parent_id,cover_id')->where([
                    'cover_id' => $get['coverid'],
                    'parent_id' => $get['parentid'],
                    'article_private' => 0 //0 公开 1 保密
                ])->select();
                if (Request::instance()->isAjax()) {
                    $result = [
                        'code' => E_ITEM_NOT_EXIST,
                        'time' => $_SERVER['REQUEST_TIME'],
                        'data' => $get,
                        'catelog_info' => $catelog_info,
                        'article_info' => $article_info
                    ];
                    return $result;
                } else {
                    $this->assign('covername', $get['covername']);
                    $this->assign('coverid', $get['coverid']);
                    $this->assign('shelfid', $get['shelfid']);
                    $this->assign('parentid', $get['parentid']);
                    $this->assign('catelog_info', $catelog_info);
                    $this->assign('article_info', $article_info);
                    return $this->fetch();
                }
            } catch (\Exception $e) {
                if (Request::instance()->isAjax()) {
                    $result = [
                        'code' => E_ITEM_NOT_EXIST,
                        'msg' => '未找到数据...',
                        'time' => $_SERVER['REQUEST_TIME'],
                        'data' => $get,
                    ];
                    return $result;
                }
            }
        } else {
            $this->redirect('home/library/index');
        }
    }

    public function getCatalog()
    {
        if (Request::instance()->isPost()) {
            $post = input('post.');
            try {
                $where = [
                    'cover_id' => $post['cover_id'],
                    'parent_id' => $post['parent_id']
                ];
                $post['catelog'] = $this->catalog->where($where)->select();
                $result = [
                    'code' => E_OK,
                    'time' => $_SERVER['REQUEST_TIME'],
                    'data' => $post
                ];
            } catch (\Exception $e) {
                $result = [
                    'code' => E_ITEM_NOT_EXIST,
                    'msg' => '未找到数据...',
                    'time' => $_SERVER['REQUEST_TIME'],
                    'data' => $post,
                ];
            }
            return $result;
        } else {
            $this->redirect('home/library/index');
        }
    }

    public function getArticle()
    {
        if (Request::instance()->isGet()) {
            $id = input('get.id');
            $this->assign('id', $id);
            return $this->fetch('article');
        } else if (Request::instance()->isAjax()) {
            try {
                $where = [
                    'id' => input('post.id'),
                ];
                $res = $this->article->where($where)->find();
                $result = [
                    'code' => E_OK,
                    'time' => $_SERVER['REQUEST_TIME'],
                    'data' => $res
                ];
            } catch (\Exception $e) {
                $result = [
                    'code' => E_ITEM_NOT_EXIST,
                    'msg' => '未找到数据...',
                    'time' => $_SERVER['REQUEST_TIME'],
                    'data' => '',
                ];
            }
            return $result;
        } else {
            $this->redirect('home/library/index');
        }
    }

    public function getArticleList()
    {
        if (Request::instance()->isPost()) {
            $post = input('post.');
            try {
                $data = $this->article->where($post)->field('article_content', true)->select();
                $result = [
                    'code' => E_OK,
                    'time' => $_SERVER['REQUEST_TIME'],
                    'data' => $data
                ];
            } catch (\Exception $e) {
                $result = [
                    'code' => E_ITEM_NOT_EXIST,
                    'msg' => '未找到数据...',
                    'time' => $_SERVER['REQUEST_TIME'],
                    'data' => [],
                ];
            }
            return $result;
        } else {
            $this->redirect('home/library/index');
        }
    }

    public function _empty($name)
    {
        $this->redirect('home/library/index');
    }

}
